import { createApp } from 'vue'
import App from './App.vue'
import MyComponent from './MyComponent'
import ('bootstrap/dist/css/bootstrap.css')

const app = createApp(App)
app.component('my-component', MyComponent)
app.mount('#app')
